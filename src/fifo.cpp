/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_FIFO_
#define _VIO_FIFO_

#include "fifo.hpp"

#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

void vio::fifo::open(file_t file, mode_t mode) {
	if (-1 == mkfifo(file, mode)) {
		if (errno != EEXIST)
			throw error(MKFIFO, "Cannot make fifo", file, strerror(errno), that);
		file::open(file);
	}
	else {
		file::open(file);
		path = file;
		state |= UNLINKING;
	}
}

void vio::fifo::host(file_t file, mode_t mode) {
	if (-1 == mkfifo(file, mode))
		throw error(MKFIFO, "Cannot make fifo", file, strerror(errno), that);
	file::open(file, O_RDONLY, 0);
	path = file;
	state |= UNLINKING;
}

void vio::fifo::make(file_t file, mode_t mode) {
	if (-1 == mkfifo(file, mode))
		throw error(MKFIFO, "Cannot make fifo", file, strerror(errno));
}

void vio::fifo::shut() {
	if (state & UNLINKING)
		unlink(path.c_str());
	file::shut();
}

vio::fifo::~fifo() {
	if (state & UNLINKING)
		unlink(path.c_str());
}

#endif
