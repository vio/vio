/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_FIFO_H
#define _VIO_FIFO_H

#include "file.hpp"

namespace vio {
	class fifo;
}

class vio::fifo : public file {
	std::string path;
	static const state_t
	UNLINKING = 0x40;
public:
	enum {
		MKFIFO,
		OPEN
	};
	fifo() : file() {
	}
	fifo(file_t file) : fifo() {
		join(file);
	}
	fifo(file_t file, mode_t mode) : fifo() {
		host(file, mode);
	}
	void open(file_t file, mode_t mode);
	void host(file_t file, mode_t mode);
	void join(file_t file) {
		file::open(file, O_WRONLY, 0);
	}
	static void make(file_t file, mode_t mode);
	void shut();
	~fifo();
};

#endif
