/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_FILE_
#define _VIO_FILE_

#include "file.hpp"

#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <vector>

void vio::file::open(file_t file, flag_t flag, mode_t mode) {
	shut();
	char* real;
	if (nullptr == (real = realpath(file, nullptr))) {
		if (!(flag & O_CREAT and ENOENT == errno))
			throw error(REALPATH, "Cannot resolve", file, strerror(errno), that);
	}
	if (-1 == (fd = ::open(real, O_NONBLOCK | flag, mode))) {
		if (real)
			free(real);
		throw error(OPEN, "Cannot open", file, strerror(errno), that);
	}
	if (real)
		free(real);
	add();
}

bool vio::file::exists(file_t file) {
	struct stat data;
	return !stat(file, &data);
}

size_t vio::file::size() {
	struct stat stat_buf;
	ssize_t rc = fstat(fd, &stat_buf);
	if (rc < 0)
		throw error(FSTAT, "Cannot stat file", strerror(errno), that);
	return stat_buf.st_size;
}

size_t vio::file::size(file_t file) {
	struct stat stat_buf;
	ssize_t rc = stat(file, &stat_buf);
	if (rc < 0)
		throw error(STAT, "Cannot stat file", file, strerror(errno));
	return stat_buf.st_size;
}

void vio::file::chmod(file_t file, mode_t mode) {
	if (-1 == ::chmod(file, mode))
		throw error(CHMOD, "Cannot change mode", file, strerror(errno));
}

void vio::file::chmod(mode_t mode) {
	if (-1 == fchmod(fd, mode))
		throw error(CHMOD, "Cannot change mode", strerror(errno), that);
}

void vio::file::mkdir(file_t file, mode_t mode) {
	if (-1 == ::mkdir(file, mode))
		throw error(MKDIR, "Cannot make directory", file, strerror(errno));
}

void vio::file::remove(file_t file) {
	if (0 != ::remove(file))
		throw error(REMOVE, "Cannot remove file", file, strerror(errno));
}

std::string vio::file::fastpath(const std::string& path, size_t slow) {
	if (path.empty())
		return "-";
	if (path.size() == 1)
		return path;
	std::string fast;
	struct item {
		size_t step, size;
		item(size_t step, size_t size) : step(step), size(size) {
		}
	};
	std::vector<item> nuke;
	nuke.reserve(slow);
	size_t step = path.size()+(path.back() == '/' ? 0 : 1), last = step;
	size_t size = 0, drop = 0, less = 0;
	while (step > 1) {
		step = path.find_last_of('/', step-2)+1;
		size = last-step;
		last = step;
		if (size == 1)
			;
		else
		if (size == 2 and path[step] == '.')
			;
		else
		if (size == 3 and path[step] == '.' and path[step+1] == '.')
			++drop;
		else
		if (drop)
			--drop;
		else
			continue;
		if (nuke.size() and step+size == nuke.back().step) {
			nuke.back().step = step;
			nuke.back().size += size;
		}
		else
			nuke.emplace_back(step, size);
		less += size;
	}
	if (path.size() > less)
		fast.reserve(path.size()-less);
	if (path.front() != '/')
		for (; drop; --drop)
			fast.append("../");
	last = 0;
	for (auto item = nuke.rbegin(); item != nuke.rend(); ++item) {
		fast.append(path, last, item->step-last);
		last = item->step+item->size;
	}
	if (last < path.size())
		fast.append(path, last, std::string::npos);
	if (fast.empty())
		fast = ".";
	else
	if (fast.size() != 1 and fast.back() == '/')
		fast.pop_back();
	return fast;
}

std::string vio::file::headpath(const std::string& path, size_t step) {
	auto base = path.size();
	for (; step; --step) {
		base = path.find_last_of('/', base-1);
		if (base == std::string::npos)
			return ".";
		if (!base)
			return "/";
	}
	return path.substr(0, base);
}

std::string vio::file::headpath(const std::string& path, const std::string& tail) {
	if (path.size() > tail.size())
		return path.substr(0, path.size()-1-tail.size());
	else
	if (path.size() < tail.size())
		throw std::string("No tail within path");
	return path.front() == '/' ? "/" : ".";
}

std::string vio::file::tailpath(const std::string& path, size_t step) {
	if (path == "/")
		return path;
	auto base = path.size();
	for (; base and base != std::string::npos and step; --step)
		base = path.find_last_of('/', base-1);
	return path.substr(base+1);
}

std::string vio::file::tailpath(const std::string& path, const std::string& head) {
	if (path.size() > head.size())
		return path.substr(head.size()+1);
	throw std::string("No head within path");
}

size_t vio::file::pathsize(const std::string& path) {
	size_t step = 0;
	for (size_t last = 0; last != std::string::npos; last = path.find('/', last+1))
		++step;
	return step;
}

#endif
