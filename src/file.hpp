/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_FILE_H
#define _VIO_FILE_H

#include "io.hpp"

#include <fcntl.h>
#include <string>

namespace vio {
	class file;
}

class vio::file : public io {
public:
	enum {
		REALPATH,
		OPEN,
		STAT,
		FSTAT,
		CHMOD,
		MKDIR,
		REMOVE
	};
	typedef const char* file_t;
	typedef int flag_t;
	file() : io() {
	}
	file(file_t file) : vio::file() {
		open(file, 0, 0);
	}
	file(file_t file, mode_t mode) : vio::file() {
		open(file, mode, 0);
	}
	virtual void open(file_t file) {
		open(file, 0, 0);
	}
	virtual void open(file_t file, mode_t mode) {
		open(file, O_CREAT, mode);
	}
	static bool exists(file_t file);
	static size_t size(file_t file);
	size_t size();
	static void chmod(file_t file, mode_t mode);
	void chmod(mode_t mode);
	static void mkdir(file_t file, mode_t mode);
	static void remove(file_t file);
	static std::string fastpath(const std::string& path, size_t slow = 6);
	static std::string headpath(const std::string& path, size_t step = 1);
	static std::string headpath(const std::string& path, const std::string& tail);
	static std::string tailpath(const std::string& path, size_t step = 1);
	static std::string tailpath(const std::string& path, const std::string& head);
	static std::string fusepath(const std::string& head, const std::string& tail) {
		return head+'/'+tail;
	}
	static size_t pathsize(const std::string& path);
protected:
	void open(file_t file, flag_t flag, mode_t mode);
};

#endif
