/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_IO_
#define _VIO_IO_

#include "io.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

vio::io::error::error(int num, const char* ctx, const char* msg, const void* ptr) {
	this->num = num;
	this->ctx = this->ctx+ctx+": "+msg;
	this->ptr = ptr;
}

vio::io::error::error(int num, const char* ctx, const char* val, const char* msg, const void* ptr) {
	this->num = num;
	this->ctx = this->ctx+ctx+" '"+val+"': "+msg;
	this->ptr = ptr;
}

const char* vio::io::error::what() const throw() {
	return ctx.c_str();
}

vio::io::error::~error() throw() {
}

/*thread_local*/ vio::io* vio::io::set::io_p = nullptr;

/*thread_local*/ bool vio::io::set::hop_v = false;

vio::io::set::set() {
	that = this;
	epfd = -1;
	events = nullptr;
	maxevents = 0;
	if (!set_p)
		use(*this);
}

vio::io::set::set(size_t maxevents) : set() {
	resize(maxevents);
	open();
}

void vio::io::set::open() {
	shut();
	if (-1 == (epfd = epoll_create1(0)))
		throw error(EPOLL_CREATE1, "epoll_create1", strerror(errno), that);
}

void vio::io::set::run() {
	while (set_p) {
		set& set_r = *set_p;
		int nfds = epoll_wait(set_r.epfd, set_r.events, set_r.maxevents, -1);
		if (-1 == nfds)
			throw error(EPOLL_WAIT, "epoll_wait", strerror(errno), set_p);
		for (int n = 0; n < nfds; ++n) {
			epoll_event& ev = set_r.events[n];
			io& io_r = *(io_p = (io*)ev.data.ptr);
			if (ev.events & EPOLLIN) {
				io_r.readable();
				if (set_r.hop_v) {
					set_r.hop_v = false;
					continue;
				}
			}
			if (ev.events & EPOLLOUT) {//maybe add ERR, HUP here?.. what if i only?
				io_r.writable();
				if (set_r.hop_v) {
					set_r.hop_v = false;
					continue;
				}
			}
			if (!(ev.events & (EPOLLIN | EPOLLOUT))) {//what if nothing... ignore error?
				ev.events & EPOLLERR ? io_r.unusable() : io_r.reusable();
				if (set_r.hop_v) {
					set_r.hop_v = false;
					continue;
				}
			}
			io_r.state &= ~(REUSABLE | UNUSABLE);
		}
	}
}

void vio::io::set::resize(size_t maxevents) {
	delete[] events;
	events = (this->maxevents = maxevents) ? new epoll_event[maxevents] : nullptr;
}

void vio::io::set::shut() {
	if (-1 != epfd)
		close(epfd);
	epfd = -1;
}

vio::io::set::~set() {
	resize();
	shut();
}

vio::io::set& vio::io::set::operator<<(io& io_r) {
	if (-1 == io_r.fd)
		throw error(INVALID_FD, "operator <<", strerror(EINVAL), that);
	if (-1 == epoll_ctl(epfd, EPOLL_CTL_ADD, io_r.fd, &io_r.ev))
		throw error(EPOLL_CTL_add, "epoll_ctl-add", strerror(errno), that);
	return *this;
}

vio::io::set& vio::io::set::operator<=(io& io_r) {
	if (-1 == io_r.fd)
		throw error(INVALID_FD, "operator <=", strerror(EINVAL), that);
	if (-1 == epoll_ctl(epfd, EPOLL_CTL_MOD, io_r.fd, &io_r.ev))
		throw error(EPOLL_CTL_mod, "epoll_ctl-mod", strerror(errno), that);
	return *this;
}

vio::io::set& vio::io::set::operator>>(io& io_r) {
	if (-1 == io_r.fd)
		throw error(INVALID_FD, "operator >>", strerror(EINVAL), that);
	if (-1 == epoll_ctl(epfd, EPOLL_CTL_DEL, io_r.fd, &io_r.ev))
		throw error(EPOLL_CTL_del, "epoll_ctl-del", strerror(errno), that);
	return *this;
}

/*thread_local*/ vio::io::set* vio::io::set_p = nullptr;

void vio::io::get(void* data, size_t size) {
	i.data = (char*)data;
	i.size = size;
	i.done = 0;
	if (i.data and state & READABLE)
		readable();
}

void vio::io::put(const void* data, size_t size) {
	o.data = (const char*)data;
	o.size = size;
	o.done = 0;
	if (o.data and state & WRITABLE)
		writable();
}

bool vio::io::nukesave() {
	bool what = state & NUKESAVE;
	state &= ~NUKESAVE;
	return what;
}

bool vio::io::pokesave() {
	bool what = state & POKESAVE;
	state &= ~POKESAVE;
	return what;
}

bool vio::io::fine(int fd) {
	return -1 != fcntl(fd, F_GETFL) or EBADF != errno;
}

void vio::io::shut() {
	if (-1 != fd)
		close(fd);
	fd = -1;
	state = 0;
	i.data = nullptr;
	o.data = nullptr;
	poker();
	nuker();
}

vio::io::~io() {
	if (-1 != fd)
		close(fd);
	poker();
	nuker();
	hop();
}

vio::io::io() : i(), o() {
	that = this;
	poker_p = nullptr;
	nuker_p = nullptr;
	closed = nullptr;
	failed = nullptr;
	poking = nullptr;
	fd = -1;
	ev.data.ptr = this;
	/*
	 * In contrast to error checking it is fine to always check for EPOLLIN and EPOLLOUT even if
	 * we are not interested in both of them since they will internally not be checked again
	 * until we emptied their buffer (really?). That is edge-triggering.
	 */
	ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
	/*
	 * Up to now, our write buffer is empty, so we are writable. The read buffer is empty, too.
	 * And that means we are not readable.
	 */
	state = WRITABLE;//mainly true for writable but not all times especially not for readable,
	//READABLE might never be triggered if no data... so will always be checked! - check for loosage?!
}

void vio::io::readable() {
	/*
	 * We are readable, so we (possibly re-)mark us as readable since it might be possible to
	 * not read until the buffer is empty and in that case we would not get re-notified since of
	 * edge-triggering. So the next time a read request is invoked, that call checks for this
	 * flag, and if it is set, we will be here again.
	 *
	 * Being here means data has just arrived, we were waiting for; or we have just invoked a
	 * read request and know that we are still readable. In both cases we are nukesave:
	 * - In the first case we have i(nput) activity, so there is no need to nuke this connection
	 *   in order of a timeout.
	 * - In the second case we have just invoked a read request and thus want to satisfy/reset
	 *   the nuking check for this moment; we want it to count from now on.
	 */
	state |= READABLE | NUKESAVE;
	/*
	 * If we are not interested for now, we will not dismiss error events, since we have just
	 * marked us and will get the error as soon as a request is invoked. Marking can only be
	 * undone here; or if we got shut, but than we do no longer care.
	 */
	if (i.data) {
		/*
		 * Determine size of outstanding data.
		 */
		size_t left = i.size-i.done;
		if (i.part and i.part < left)
			/*
			 * There is much data, but we want to read it in smaller chunks to prevent
			 * starvation of other file descriptors.
			 */
			left = i.part;
		do {
			int what = read(fd, i.data+i.done, left);
			if (!(what > 0)) {
				if (-1 == what and (errno == EAGAIN or errno == EWOULDBLOCK)) {
					/*
					 * Buffer empty, no longer readable.
					 */
					state &= ~READABLE;
					/*
					 * Break out of loop in order of empty buffer. Even break out of
					 * function if the previous get() emptied the buffer since we can
					 * only now determine if we are still readable but we do not want
					 * the callback function to be called without progress.
					 */
					if (i.done)
						break;
					else
						return;
				}
				else {
					/*
					 * If 0 was returned, we were closed, but might be reusable.
					 * Otherwise, an error occured and we are unsusable.
					 */
					what ? unusable() : reusable();
					/*
					 * Break out of function in order of an error.
					 */
					return;
				}
			}

			/*
			 * Sum up what we have read so far and what is still left.
			 */
			i.done += what;
			left -= what;
		}
		while (left);

		/*
		 * We made progress, call back.
		 */
		if (i.come)
			i.come(i.done, i.size);
		if (i.done == i.size) {
			/*
			 * We are done, the request is gone, call back.
			 */
			i.data = nullptr;
			if (i.gone)
				i.gone();
		}
	}
}

void vio::io::writable() {
	/*
	 * We are writable, so we (possibly re-)mark us as writable since it is common to not
	 * completely fill the buffer and in that case we would not get re-notified since of
	 * edge-triggering. So the next time a write request is invoked, that call checks for this
	 * flag, and if it is set, we will be here again.
	 *
	 * Being here means the buffer has just become writable again, we were waiting for; or we
	 * have just invoked a write request and know that we are still writable. In both cases we
	 * are pokesave:
	 * - In the first case we will just create o(utput) activity now, so there is no need to
	 *   additionally send keep alive packets.
	 * - In the second case we have just invoked a write request and thus want to satisfy/reset
	 *   the poking check for this moment; we want it to count from now on.
	 */
	state |= WRITABLE | POKESAVE;
	/*
	 * If we are not interested for now, we will not dismiss error events, since we have just
	 * marked us and will get the error as soon as a request is invoked. Marking can only be
	 * undone here; or if we got shut, but than we do no longer care.
	 */
	if (o.data) {
		/*
		 * Determine size of outstanding data.
		 */
		size_t left = o.size-o.done;
		if (o.part and o.part < left)
			/*
			 * There is much data, but we want to write it in smaller chunks to prevent
			 * starvation of other file descriptors.
			 */
			left = o.part;
		do {
			int what = write(fd, o.data+o.done, left);
			if (!(what > 0)) {
				if (-1 == what and (errno == EAGAIN or errno == EWOULDBLOCK)) {
					/*
					 * Buffer full, no longer writable.
					 */
					state &= ~WRITABLE;
					/*
					 * Break out of loop in order of full buffer. Even break out of
					 * function if the previous put() completely filled the buffer since
					 * we can only now determine if we are still writable but we do not
					 * want the callback function to be called without progress.
					 */
					if (o.done)
						break;
					else
						return;
				}
				else {
					/*
					 * An error occured and we are unsusable.
					 * We treat 0 as an error, too.
					 */
					unusable();
					/*
					 * Break out of function in order of an error.
					 */
					return;
				}
			}
			/*
			 * Sum up what we have written so far and what is still left.
			 */
			o.done += what;
			left -= what;
		}
		while (left);
		/*
		 * We made progress, call back.
		 */
		if (o.come)
			o.come(o.done, o.size);
		if (o.done == o.size) {
			/*
			 * We are done, the request is gone, call back.
			 */
			o.data = nullptr;
			if (o.gone)
				o.gone();
		}
	}
}

void vio::io::reusable() {
	if (!(state & REUSABLE)) {
		state |= REUSABLE;
		if (closed)
			closed();
	}
}

void vio::io::unusable() {
	if (!(state & UNUSABLE)) {
		shut();
		state |= UNUSABLE;
		if (failed)
			failed();
	}
}

void vio::io::set_non_blocking() {
	int flags;
	if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	if (-1 == fcntl(fd, F_SETFL, flags | O_NONBLOCK))
		throw error(FCNTL, "fcntl", strerror(errno), that);
}

#endif
