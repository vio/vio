/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_IO_H
#define _VIO_IO_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <functional>
#include <sys/epoll.h>
#include <exception>
#include <string>
#include <list>

namespace vio {
	using namespace std::placeholders;
}

namespace vio {
	class io;
}

class vio::io {
	io(const io&) = delete;
	io(io&&) = delete;
	io& operator=(const io&) = delete;
	io& operator=(io&&) = delete;
public:
	enum ERROR {
		FCNTL
	};
	class error : public std::exception {
	public:
		std::string ctx;
		const void* ptr;
		int num;
		error(int num, const char* ctx, const char* msg, const void* ptr = nullptr);
		error(int num, const char* ctx, const char* val, const char* msg, const void* ptr = nullptr);
		virtual const char* what() const throw();
		virtual ~error() throw();
	};
	class set {
		set(const set&) = delete;
		set(set&&) = delete;
		set& operator=(const set&) = delete;
		set& operator=(set&&) = delete;
	public:
		enum ERROR {
			EPOLL_CREATE1,
			EPOLL_WAIT,
			EPOLL_CTL_add,
			EPOLL_CTL_mod,
			EPOLL_CTL_del,
			INVALID_FD,
		};
		set* that;
		set();
		set(size_t maxevents);
		void resize(size_t maxevents = 0);
		void open();
		void shut();
		static void use() {
			set_p = nullptr;
		}
		static void use(set& set_r) {
			set_p = &set_r;
		}
		static bool now(io& io_r) {
			if (set_p)
				return set_p->io_p == &io_r;
			return false;
		}
		static void hop() {
			set_p->hop_v = true;
		}
		static void hop(io& io_r) {
			if (now(io_r))
				hop();
		}
		static void run();
		virtual ~set();
		set& operator<<(io& io_r);
		set& operator<=(io& io_r);
		set& operator>>(io& io_r);
	private:
		int epfd;
		int maxevents;
		epoll_event* events;
		/*thread_local*/ static io* io_p;
		/*thread_local*/ static bool hop_v;
	};
	io* that;
	typedef std::function<void(size_t done, size_t size)> come_t;
	typedef std::function<void()> gone_t;
	class x_t {
	protected:
		size_t size;
		size_t done;
		size_t part;
	public:
		come_t come;
		gone_t gone;
	};
	class i_t : public x_t {
	friend io;
	protected:
		char* data;
	} i;
	class o_t : public x_t {
	friend io;
	protected:
		const char* data;
	} o;
	typedef std::function<void()> closed_t;
	typedef std::function<void()> failed_t;
	typedef std::function<void()> poking_t;
	typedef std::function<void()> nuking_t;
	closed_t closed;
	failed_t failed;
	poking_t poking;
	nuking_t nuking;
	void get(come_t come, gone_t gone) {
		i.come = come;
		i.gone = gone;
	}
	void get(void* data = nullptr, size_t size = 0);
	void get(void* data, size_t size, gone_t gone) {
		i.gone = gone;
		get(data, size);
	}
	void put(come_t come, gone_t gone) {
		o.come = come;
		o.gone = gone;
	}
	void put(const void* data = nullptr, size_t size = 0);
	void put(const void* data, size_t size, gone_t gone) {
		o.gone = gone;
		put(data, size);
	}
	bool nukesave();
	bool pokesave();
	void nuker(io** io_pp = nullptr) {
		if (nuker_p)
			*nuker_p = nullptr;
		nuker_p = io_pp;
	}
	void poker(io** io_pp = nullptr) {
		if (poker_p)
			*poker_p = nullptr;
		poker_p = io_pp;
	}
	void nuke() {
		nuker();
		if (nuking)
			nuking();
	}
	void poke() {
		if (poking)
			poking();
	}
	bool fine() {
		return fine(fd);
	}
	static bool fine(int fd);
	virtual void shut();
	virtual ~io();
protected:
	io();
	virtual void readable();
	virtual void writable();
	virtual void reusable();
	virtual void unusable();
	void set_non_blocking();
	int fd;
	typedef unsigned char state_t;
	state_t state;
	static const state_t
	NUKESAVE = 0x01,
	POKESAVE = 0x02,
	READABLE = 0x04,
	WRITABLE = 0x08,
	REUSABLE = 0x10,
	UNUSABLE = 0x20;
	/*thread_local*/ static set* set_p;
	void add() {
		if (set_p)
			*set_p << *this;
	}
	void mod() {
		if (set_p)
			*set_p <= *this;
	}
	void del() {
		if (set_p)
			*set_p >> *this;
	}
	bool now() {
		return set::now(*this);
	}
	void hop() {
		set::hop(*this);
	}
private:
	epoll_event ev;
	io** poker_p;
	io** nuker_p;
};

#endif
