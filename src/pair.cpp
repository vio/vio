/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_PAIR_
#define _VIO_PAIR_

#include "pair.hpp"

#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

vio::pair::pair() : io() {
}

vio::pair::pair(forked_t forked) : pair() {
	fork(forked);
}

vio::pair::pair(forked_t forked, pid_t& pid) : pair() {
	pid = fork(forked);
}

pid_t vio::pair::fork(forked_t forked) {
	int fds[2];
	if (-1 == socketpair(AF_LOCAL, SOCK_STREAM, SOCK_NONBLOCK, fds))
		throw error(SOCKETPAIR, "Cannot create socket pair", strerror(errno), that);
	pid_t pid;
	if (-1 == (pid = ::fork()))
		throw error(FORK, "Cannot fork process", strerror(errno), that);
	if (pid) {
		fd = fds[0];
		close(fds[1]);
		add();
		return pid;
	}
	else {
		fd = fds[1];
		close(fds[0]);
		if (set_p) {
			set_p->open();
			*set_p << *this;
		}
		if (forked)
			forked();
		exit(EXIT_SUCCESS);
	}
}

#endif
