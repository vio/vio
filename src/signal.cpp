/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_SIGNAL_
#define _VIO_SIGNAL_

#include "signal.hpp"

#include <errno.h>
#include <string.h>

vio::signal::signal() : io() {
	come = nullptr;
	non();
}

vio::signal::signal(come_t come, int signum) : signal() {
	this->come = come;
	open(signum);
}

void vio::signal::non() {
	if (-1 == sigemptyset(&sigset))
		throw error(SIGEMPTYSET, "sigemptyset", strerror(errno), that);
}

void vio::signal::all() {
	if (-1 == sigfillset(&sigset))
		throw error(SIGFILLSET, "sigfillset", strerror(errno), that);
}

vio::signal& vio::signal::operator<<(int signum) {
	if (-1 == sigaddset(&sigset, signum))
		throw error(SIGADDSET, "sigaddset", strerror(errno), that);
	return *this;
}


vio::signal& vio::signal::operator>>(int signum) {
	if (-1 == sigdelset(&sigset, signum))
		throw error(SIGDELSET, "sigdelset", strerror(errno), that);
	return *this;
}

void vio::signal::open() {
	shut();
	if (-1 == (fd = signalfd(fd, &sigset, SFD_NONBLOCK)))
		throw error(SIGNALFD, "signalfd", strerror(errno), that);
	add();
	if (-1 == sigprocmask(SIG_SETMASK, &sigset, nullptr))
		throw error(SIGPROCMASK, "sigprocmask", strerror(errno));
	get(&siginfo, sizeof siginfo, std::bind(&signal::got, this));
}

void vio::signal::open(int signum) {
	*this << signum;
	open();
}

void vio::signal::got() {
	if (come)
		come(siginfo);
	get(&siginfo, sizeof siginfo);
}

void vio::signal::own() {
	sigset_t sigset;
	if (-1 == sigfillset(&sigset))
		throw error(SIGFILLSET, "sigfillset", strerror(errno));
	if (-1 == sigprocmask(SIG_SETMASK, &sigset, nullptr))
		throw error(SIGPROCMASK, "sigprocmask", strerror(errno));
}

#endif
