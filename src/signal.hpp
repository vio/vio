/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_SIGNAL_H
#define _VIO_SIGNAL_H

#include "io.hpp"

#include <signal.h>
#include <sys/signalfd.h>

namespace vio {
	class signal;
}

class vio::signal : public io {
public:
	enum ERROR {
		SIGNALFD,
		SIGFILLSET,
		SIGPROCMASK,
		SIGEMPTYSET,
		SIGACTION,
		SIGADDSET,
		SIGDELSET
	};
	typedef std::function<void(const signalfd_siginfo&)> come_t;
	come_t come;
	signal();
	signal(come_t come, int signum);
	void non();
	void all();
	signal& operator<<(int signum);
	signal& operator>>(int signum);
	void open();
	void open(int signum);
	static void own();
private:
	void got();
	sigset_t sigset;
	signalfd_siginfo siginfo;
};

#endif
