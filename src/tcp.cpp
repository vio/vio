/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>      *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_TCP_
#define _VIO_TCP_

#include "tcp.hpp"

#include <unistd.h>
#include <errno.h>
#include <string.h>

/*thread_local*/ vio::tcp::subs_t vio::tcp::subs;

vio::tcp::tcp() : io() {
	subset_p = nullptr;
	ai = nullptr;
	size = sizeof data;
	hosted = nullptr;
	joined = nullptr;
	denied = nullptr;
}

void vio::tcp::subset(set& set_r) {
	subset_p = &set_r;
}

void vio::tcp::drop() {
	subs.erase(sub_p);
}

vio::tcp::tcp(port_t port, hosted_t hosted) : tcp() {
	this->hosted = hosted;
	host(port);
}

vio::tcp::tcp(host_t host, port_t port, joined_t joined, denied_t denied) : tcp() {
	this->joined = joined;
	this->denied = denied;
	join(host, port);
}

std::tuple<std::string, std::string> vio::tcp::info(info_flag_t flag) {
	size = sizeof data;
	char host[NI_MAXHOST];
	char port[NI_MAXSERV];
	if (int gaino = getnameinfo((sockaddr*)&data, size, host, sizeof host, port, sizeof port, flag))
		throw error(GETNAMEINFO, "getnameinfo", gai_strerror(gaino), that);
	return std::make_tuple(host, port);
}

void vio::tcp::join(host_t host, port_t port) {
	shut();
	addrinfo hi = { 0, 0, 0, 0, 0, 0, 0, 0 };
	hi.ai_flags = AI_ADDRCONFIG;
	hi.ai_socktype = SOCK_STREAM;
	if(int gaino = getaddrinfo(host, port, &hi, &ai))
		throw error(GETADDRINFO, "getaddrinfo", gai_strerror(gaino), that);
	state |= ESTABLISH | NUKESAVE | POKESAVE;
	join();
}

void vio::tcp::join() {
	for (; ai; ai = ai->ai_next) {
		if (-1 == (fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)))
			continue;
		add();
		set_non_blocking();
		if (-1 == connect(fd, ai->ai_addr, ai->ai_addrlen)) {
			if (errno == EINPROGRESS) {
				ai = ai->ai_next;
				return;
			}
			close(fd);
			continue;
		}
		freeaddrinfo(ai);
		set_non_blocking();
		state &= ~ESTABLISH;
		if (joined)
			joined();
		return;
	}
	shut();
	if (denied)
		denied(error(CONNECT, "connect", "No addresses left", that));
}

void vio::tcp::host(port_t port, bool reuse) {
	shut();
	int value = reuse ? 1 : 0;
	state |= LISTENING | NUKESAVE;
	addrinfo* ai, hi = { 0, 0, 0, 0, 0, 0, 0, 0 };
	hi.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;
	hi.ai_socktype = SOCK_STREAM;
	if (int gaino = getaddrinfo(nullptr, port, &hi, &ai))
		throw error(GETADDRINFO, "getaddrinfo", gai_strerror(gaino), that);
	for (; ai; ai = ai->ai_next) {
		if (-1 == (fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)))
			continue;
		if (reuse and -1 == setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &value, sizeof value))
			throw error(SETSOCKOPT, "setsockopt", strerror(errno), that);
		if (-1 == bind(fd, ai->ai_addr, ai->ai_addrlen)) {
			close(fd);
			fd = -1;
			continue;
		}
	}
	freeaddrinfo(ai);
	if (-1 == fd)
		throw error(BIND, "bind", strerror(errno), that);
	if (-1 == listen(fd, SOMAXCONN))
		throw error(LISTEN, "listen", strerror(errno), that);
	add();
	set_non_blocking();
}

void vio::tcp::readable() {
	if (state & LISTENING) {
		state |= READABLE | NUKESAVE;
		int subfd = -1;
		sockaddr_storage data;
		socklen_t size = sizeof data;
		while (true) {
			if (-1 == (subfd = accept(fd, (sockaddr*)&data, &size))) {
				if (errno == ECONNABORTED or errno == EAGAIN or errno == EWOULDBLOCK)
					break;
				else
					throw error(ACCEPT, "accept", strerror(errno), that);
			}
			else {
				if (-1 != getpeername(subfd, (sockaddr*)&data, &size)) {
					subs.emplace_front();
					tcp& tcp_r = subs.front();
					tcp_r.sub_p = subs.begin();
					tcp_r.fd = subfd;
					tcp_r.data = data;
					tcp_r.size = size;
					tcp_r.state |= NUKESAVE | POKESAVE;
					tcp_r.set_non_blocking();
					if (subset_p)
						*subset_p << tcp_r;
					else
						tcp_r.add();
					if (hosted)
						hosted(tcp_r);
				}
			}
		}
	}
	else
		io::readable();
}

void vio::tcp::writable() {
	if (state & ESTABLISH) {
		state |= WRITABLE | NUKESAVE | POKESAVE;
		int what = 0;
		socklen_t size = sizeof what;
		if (-1 == getsockopt(fd, SOL_SOCKET, SO_ERROR, &what, &size)) {
			shut();
			denied(error(GETSOCKOPT, "getsockopt", strerror(errno), that));
		}
		if (what) {
			close(fd);
			if (ai)
				join();
			else {
				shut();
				if (denied)
					denied(error(CONNECT, "connect", strerror(what), that));
			}
		}
		else {
			freeaddrinfo(ai);
			set_non_blocking();
			state &= ~ESTABLISH;
			if (joined)
				joined();
		}
	}
	else
		io::writable();
}

#endif
