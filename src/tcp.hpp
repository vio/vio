/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <n3vu0r@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_TCP_H
#define _VIO_TCP_H

#include "io.hpp"

#include <sys/socket.h>
#include <netdb.h>
#include <list>
#include <tuple>
#include <string>

namespace vio {
	class tcp;
}

class vio::tcp : public io {
public:
	enum {
		CONNECT,
		GETADDRINFO,
		GETSOCKOPT,
		GETNAMEINFO,
		BIND,
		LISTEN,
		SETSOCKOPT,
		GETPEERNAME,
		ACCEPT
	};
	typedef int info_flag_t;
	static const info_flag_t
	REQUIRE_NAME = NI_NAMEREQD,
	SHORTEN_FQDN = NI_NOFQDN,
	NUMERIC_HOST = NI_NUMERICHOST,
	NUMERIC_PORT = NI_NUMERICSERV,
	NUMERIC_INFO = NUMERIC_HOST | NUMERIC_PORT;
	typedef std::function<void(tcp& tcp_r)> hosted_t;
	typedef std::function<void()> joined_t;
	typedef std::function<void(const error& error)> denied_t;
	typedef const char* host_t;
	typedef const char* port_t;
	tcp();
	tcp(port_t port, hosted_t hosted);
	tcp(host_t host, port_t port, joined_t joined, denied_t denied);
	void subset(set& set_r);
	void join(host_t host, port_t port);
	void host(port_t port, bool reuse = true);
	std::tuple<std::string, std::string> info(info_flag_t flag = 0);
	void drop();
	hosted_t hosted;
	joined_t joined;
	denied_t denied;
private:
	typedef std::list<tcp> subs_t;
	/*thread_local*/ static subs_t subs;
	subs_t::iterator sub_p;
	io::set* subset_p;
	sockaddr_storage data;
	socklen_t size;
	addrinfo* ai;
	void join();
	static const state_t
	ESTABLISH = 0x40,
	LISTENING = 0x80;
	void readable();
	void writable();
};

#endif
