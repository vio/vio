/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_TIMER_
#define _VIO_TIMER_

#include "timer.hpp"

#include <errno.h>
#include <string.h>

vio::timer::timer() : io() {
	base = VIO_TIMER_DEFAULT_BASE;
	come = nullptr;
}

vio::timer::timer(time span, clockid_t base) : timer() {
	open(base);
	poke(span);
}

vio::timer::timer(come_t come, time span, clockid_t base) : timer(span, base) {
	this->come = come;
}

void vio::timer::open() {
	open(base);
}

void vio::timer::open(clockid_t base) {
	shut();
	if (-1 == (fd = timerfd_create(base, TFD_NONBLOCK)))
		throw error(TIMERFD_CREATE, "timerfd_create", strerror(errno), that);
	add();
	get(&expirations, sizeof expirations, std::bind(&timer::got, this));
	last.get(base);
}

void vio::timer::poke(time span) {
	itimerspec value = { span, span };
	if (-1 == timerfd_settime(fd, 0, &value, nullptr))
		throw error(TIMERFD_SETTIME, "timerfd_settime", strerror(errno), that);
}

void vio::timer::wait(time span, done_t done) {
	waits.emplace_back(last, span, done);
}

void vio::timer::nuke(time span, io& io_r) {
	nukes.emplace_back(last, span, io_r);
	io_r.nuker(&nukes.back().io_p);
}

void vio::timer::poke(time span, io& io_r) {
	pokes.emplace_back(last, span, io_r);
	io_r.poker(&pokes.back().io_p);
}

void vio::timer::got() {
	last.get(base);
	for (auto wait = waits.begin(); wait != waits.end();) {
		if (last < wait->last+wait->span)
			++wait;
		else {
			bool drop = false;
			wait->done(drop);
			if (drop)
				wait = waits.erase(wait);
			else
				wait->last = last;
		}
	}
	for (auto nuke = nukes.begin(); nuke != nukes.end();) {
		if (nuke->io_p) {
			if (nuke->io_p->nukesave())
				nuke->last = last;
			else {
				if (last < nuke->last+nuke->span)
					++nuke;
				else {
					nuke->io_p->nuke();
					nuke = nukes.erase(nuke);
				}
			}
		}
		else
			nuke = nukes.erase(nuke);
	}
	for (auto poke = pokes.begin(); poke != pokes.end();) {
		if (poke->io_p) {
			if (poke->io_p->pokesave())
				poke->last = last;
			else {
				if (last < poke->last+poke->span)
					++poke;
				else
					poke->io_p->poke();
			}
		}
		else
			poke = pokes.erase(poke);
	}
	if (come)
		come(expirations);
	get(&expirations, sizeof expirations);
}

#endif
