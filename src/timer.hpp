/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2013  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_TIMER_H
#define _VIO_TIMER_H

#include "io.hpp"

#include <sys/timerfd.h>
#include <string.h>
#include <errno.h>
#include <list>

#define VIO_TIMER_DEFAULT_BASE CLOCK_REALTIME

namespace vio {
	class timer;
}

class vio::timer : public io {
public:
	enum ERROR {
		TIMERFD_CREATE,
		TIMERFD_SETTIME,
		CLOCK_GETTIME
	};
	struct time : public timespec {
		time() : timespec() {
		}
		time(time_t sec, long nsec = 0) {
			tv_sec = sec;
			tv_nsec = nsec;
		}
		bool operator==(const time& time) const {
			return tv_sec == time.tv_sec and tv_nsec == time.tv_nsec;
		}
		bool operator<(const time& time) const {
			return tv_sec == time.tv_sec
			? tv_nsec < time.tv_nsec
			: tv_sec < time.tv_sec;
		}
		bool operator<=(const time& time) const {
			return tv_sec == time.tv_sec
			? tv_nsec <= time.tv_nsec
			: tv_sec <= time.tv_sec;
		}
		bool operator>(const time& time) const {
			return tv_sec == time.tv_sec
			? tv_nsec > time.tv_nsec
			: tv_sec > time.tv_sec;
		}
		bool operator>=(const time& time) const {
			return tv_sec == time.tv_sec
			? tv_nsec >= time.tv_nsec
			: tv_sec >= time.tv_sec;
		}
		time operator+(const time& time) const {
			timer::time copy(*this);
			copy.tv_sec += time.tv_sec;
			copy.tv_nsec += time.tv_nsec;
			if (copy.tv_nsec >= 1000000000L) {
				++copy.tv_sec;
				copy.tv_nsec = copy.tv_nsec-1000000000L;
			}
			return copy;
		}
		time operator-(const time& time) const {
			timer::time copy(*this);
			if (
				tv_sec < time.tv_sec
				or (
					tv_sec == time.tv_sec
					and
					tv_nsec <= time.tv_nsec
				)
			) {
				copy.tv_sec = 0;
				copy.tv_nsec = 0;
			}
			else {
				copy.tv_sec = tv_sec-time.tv_sec;
				if (tv_nsec < time.tv_nsec) {
					copy.tv_nsec = tv_nsec+1000000000L-time.tv_nsec;
					--copy.tv_sec;
				}
				else
					copy.tv_nsec = tv_nsec-time.tv_nsec;
			}
			return copy;
		}
		void get(clockid_t base = VIO_TIMER_DEFAULT_BASE) {
			if (-1 == clock_gettime(base, this))
				throw error(CLOCK_GETTIME, "clock_gettime", strerror(errno), this);
		}
	};
	typedef std::function<void(uint64_t expirations)> come_t;
	timer();
	timer(time span, clockid_t base = VIO_TIMER_DEFAULT_BASE);
	timer(come_t come, time span, clockid_t base = VIO_TIMER_DEFAULT_BASE);
	void poke(time span = 0);
	void halt() {
		poke();
	}
	void open();
	void open(clockid_t base);
	typedef std::function<void(bool& drop)> done_t;
	void wait(time span, done_t done);
	void nuke(time span, io& io_r);
	void poke(time span, io& io_r);
	clockid_t base;
	come_t come;
private:
	uint64_t expirations;
	time last;
	struct ev_t {
		time last;
		time span;
		ev_t(time last, time span) {
			this->last = last;
			this->span = span;
		}
	};
	struct cb_t : public ev_t {
		done_t done;
		cb_t(time last, time span, done_t done) : ev_t(last, span) {
			this->done = done;
		}
	};
	struct io_t : public ev_t {
		io* io_p;
		io_t(time last, time span, io& io_r) : ev_t(last, span) {
			io_p = &io_r;
		}
	};
	std::list<cb_t> waits;
	std::list<io_t> nukes;
	std::list<io_t> pokes;
	void got();
};

#endif
