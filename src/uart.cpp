/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <n3vu0r@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_UART_
#define _VIO_UART_

#include "uart.hpp"

#include <errno.h>
#include <string.h>
#include <fcntl.h>

vio::uart::uart() : file(), cf() {
	set_baud();
	set_mode();
}

vio::uart::uart(file_t file) : uart() {
	open(file);
}

vio::uart::uart(file_t file, baud_t baud) : uart() {
	open(file, baud);
}

vio::uart::uart(file_t file, mode_t mode) : uart() {
	open(file, mode);
}

vio::uart::uart(file_t file, baud_t baud, mode_t mode) : uart() {
	open(file, baud, mode);
}

void vio::uart::set_baud(baud_t baud) {
	if (-1 == baud)
		baud = BAUD;
	else
		switch (baud) {
			case       0: baud =       B0; break;
			case      50: baud =      B50; break;
			case      75: baud =      B75; break;
			case     110: baud =     B110; break;
			case     134: baud =     B134; break;
			case     150: baud =     B150; break;
			case     200: baud =     B200; break;
			case     300: baud =     B300; break;
			case     600: baud =     B600; break;
			case    1200: baud =    B1200; break;
			case    1800: baud =    B1800; break;
			case    2400: baud =    B2400; break;
			case    4800: baud =    B4800; break;
			case    9600: baud =    B9600; break;
			case   19200: baud =   B19200; break;
			case   38400: baud =   B38400; break;
			case   57600: baud =   B57600; break;
			case  115200: baud =  B115200; break;
			case  230400: baud =  B230400; break;
			case  460800: baud =  B460800; break;
			case  500000: baud =  B500000; break;
			case  576000: baud =  B576000; break;
			case  921600: baud =  B921600; break;
			case 1000000: baud = B1000000; break;
			default:
				throw error(UNSUPPORTED_BAUD, "Cannot set baud to", std::to_string(baud).c_str(), "Unsupported baud", that);
			break;
		}
	if (-1 == cfsetispeed(&cf, baud))
		throw error(CFSETISPEED, "Cannot set input speed", strerror(errno), that);
	if (-1 == cfsetospeed(&cf, baud))
		throw error(CFSETOSPEED, "Cannot set output speed", strerror(errno), that);
}

void vio::uart::set_mode(mode_t mode) {
	cf.c_cflag = CREAD | CLOCAL;
	if (!mode)
		cf.c_cflag |= MODE;
	else {
		if (3 != strlen(mode))
			throw error(UNSUPPORTED_MODE, "Cannot set mode to", mode, "Unsupported mode", that);
		switch (mode[0]) {
			case '5': cf.c_cflag |= CS5; break;
			case '6': cf.c_cflag |= CS6; break;
			case '7': cf.c_cflag |= CS7; break;
			case '8': cf.c_cflag |= CS8; break;
			default:
				throw error(UNSUPPORTED_SIZE, "Cannot set mode to", mode, "Unsupported size", that);
			break;
		}
		switch (mode[1]) {
			case 'N': case 'n': break;
			case 'O': case 'o': cf.c_cflag |= PARENB | PARODD; break;
			case 'E': case 'e': cf.c_cflag |= PARENB; break;
			default:
				throw error(UNSUPPORTED_PARITY, "Cannot set mode to", mode, "Unsupported parity", that);
			break;
		}
		switch (mode[2]) {
			case '1': break;
			case '2': cf.c_cflag |= CSTOPB; break;
			default:
				throw error(UNSUPPORTED_STOP_BIT, "Cannot set mode to", mode, "Unsupported stop bit", that);
			break;
		}
	}
}

void vio::uart::open(file_t file) {
	shut();
	if (-1 == (fd = ::open(file, O_RDWR | O_NOCTTY | O_NDELAY)))
		throw error(OPEN, "Cannot open", file, strerror(errno), that);
	tune();
	add();
}

void vio::uart::tune() {
	if (-1 == fd)
		throw error(INVALID_FD, "Cannot tune", "Invalid file descriptor", that);
	if (-1 == tcsetattr(fd, TCSANOW, &cf))
		throw error(TCSETATTR, "Cannot set terminal parameters", strerror(errno), that);
}

#endif
