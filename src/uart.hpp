/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013  Rouven Spreckels  <n3vu0r@nevux.org>            *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_UART_H
#define _VIO_UART_H

#include "file.hpp"

#include <termios.h>

namespace vio {
	class uart;
}

class vio::uart : public file {
	termios cf;
	void open(file_t, mode_t) {
	}
public:
	enum {
		UNSUPPORTED_BAUD,
		CFSETISPEED,
		CFSETOSPEED,
		UNSUPPORTED_MODE,
		UNSUPPORTED_SIZE,
		UNSUPPORTED_PARITY,
		UNSUPPORTED_STOP_BIT,
		INVALID_FD,
		OPEN,
		TCSETATTR
	};
	typedef const char* file_t;
	typedef int baud_t;
	typedef const char* mode_t;
	uart();
	uart(file_t file);
	uart(file_t file, baud_t baud);
	uart(file_t file, mode_t mode);
	uart(file_t file, baud_t baud, mode_t mode);
	void open(file_t file);
	void open(file_t file, baud_t baud) {
		set_baud(baud);
		open(file);
	}
	void open(file_t file, mode_t mode) {
		set_mode(mode);
		open(file);
	}
	void open(file_t file, baud_t baud, mode_t mode) {
		set_baud(baud);
		set_mode(mode);
		open(file);
	}
	void tune(baud_t baud) {
		set_baud(baud);
		tune();
	}
	void tune(mode_t mode) {
		set_mode(mode);
		tune();
	}
	void tune(baud_t baud, mode_t mode) {
		set_baud(baud);
		set_mode(mode);
		tune();
	}
private:
	void set_baud(baud_t baud = -1);
	void set_mode(mode_t mode = 0);
	void tune();
	static const int BAUD = B9600;
	static const tcflag_t MODE = CS8;
};

#endif
