/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>      *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_UDS_
#define _VIO_UDS_

#include "uds.hpp"

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <string>

/*thread_local*/ vio::uds::subs_t vio::uds::subs;

vio::uds::uds() : file() {
	subset_p = nullptr;
	data.sun_family = AF_UNIX;
	size = 0;
	hosted = nullptr;
	joined = nullptr;
	denied = nullptr;
}

void vio::uds::subset(set& set_r) {
	subset_p = &set_r;
}

void vio::uds::drop() {
	subs.erase(sub_p);
}

vio::uds::uds(file_t file, joined_t joined, denied_t denied) : uds() {
	this->joined = joined;
	this->denied = denied;
	join(file);
}

vio::uds::uds(file_t file, hosted_t hosted) : uds() {
	this->hosted = hosted;
	host(file);
}

vio::uds::uds(file_t file, mode_t mode, hosted_t hosted) : uds() {
	this->hosted = hosted;
	host(file, mode);
}

void vio::uds::join(file_t file) {
	size = strlen(file);
	if (!(size < sizeof data.sun_path))
		throw error(FILEPATH, "filepath", "File path is too long", that);
	shut();
	if (-1 == (fd = socket(AF_UNIX, SOCK_STREAM, 0)))
		throw error(SOCKET, "socket", strerror(errno), that);
	add();
	set_non_blocking();
	strcpy(data.sun_path, file);
	state |= ESTABLISH | NUKESAVE | POKESAVE;
	if (-1 == connect(fd, (sockaddr*)&data, size+sizeof data.sun_family)) {
		if (errno != EINPROGRESS) {
			shut();
			if (denied)
				denied(error(CONNECT, "connect", strerror(errno), that));
		}
	}
	else {
		set_non_blocking();
		state &= ~ESTABLISH;
		if (joined)
			joined();
	}
}

void vio::uds::host(file_t file) {
	size = strlen(file);
	if (!(size < sizeof data.sun_path))
		throw error(FILEPATH, "filepath", "File path is too long", that);
	shut();
	if (-1 == (fd = socket(AF_UNIX, SOCK_STREAM, 0)))
		throw error(SOCKET, "socket", strerror(errno), that);
	strcpy(data.sun_path, file);
	if (-1 == bind(fd, (sockaddr*)&data, size+sizeof data.sun_family))
		throw error(BIND, "bind", strerror(errno), that);
	if (-1 == listen(fd, SOMAXCONN))
		throw error(LISTEN, "listen", strerror(errno), that);
	add();
	set_non_blocking();
	state |= LISTENING | NUKESAVE;
}

void vio::uds::host(file_t file, mode_t mode) {
	std::string path(file);
	chmod(headpath(fastpath(path)).c_str(), mode);
	host(file);
}

void vio::uds::shut() {
	if (state & LISTENING)
		unlink(data.sun_path);
	file::shut();
}

vio::uds::~uds() {
	if (state & LISTENING)
		unlink(data.sun_path);
}

void vio::uds::readable() {
	if (state & LISTENING) {
		state |= READABLE | NUKESAVE;
		int subfd = -1;
		sockaddr_un data;
		socklen_t size = sizeof data;
		while (true) {
			if (-1 == (subfd = accept(fd, (sockaddr*)&data, &size))) {
				if (errno == ECONNABORTED or errno == EAGAIN or errno == EWOULDBLOCK)
					break;
				else
					throw error(ACCEPT, "accept", strerror(errno), that);
			}
			else {
				subs.emplace_front();
				uds& uds_r = subs.front();
				uds_r.sub_p = subs.begin();
				uds_r.fd = subfd;
				uds_r.state |= NUKESAVE | POKESAVE;
				uds_r.set_non_blocking();
				if (subset_p)
					*subset_p << uds_r;
				else
					uds_r.add();
				if (hosted)
					hosted(uds_r);
				
			}
		}
	}
	else
		io::readable();
}

void vio::uds::writable() {
	if (state & ESTABLISH) {
		state |= WRITABLE | NUKESAVE | POKESAVE;
		int what = 0;
		socklen_t size = sizeof what;
		if (-1 == getsockopt(fd, SOL_SOCKET, SO_ERROR, &what, &size)) {
			shut();
			denied(error(GETSOCKOPT, "getsockopt", strerror(errno), that));
		}
		if (what) {
			shut();
			if (denied)
				denied(error(CONNECT, "connect", strerror(what), that));
		}
		else {
			set_non_blocking();
			state &= ~ESTABLISH;
			if (joined)
				joined();
		}
	}
	else
		io::writable();
}

#endif
