/******************************************************************************
 *  This file is part of vio.                                                 *
 *                                                                            *
 *  Copyright (C) 2012, 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>      *
 *                                                                            *
 *  vio is free software: you can redistribute it and/or modify               *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  vio is distributed in the hope that it will be useful,                    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with vio.  If not, see <http://www.gnu.org/licenses/>.              *
 ******************************************************************************/

#ifndef _VIO_UDS_H
#define _VIO_UDS_H

#include "file.hpp"

#include <sys/socket.h>
#include <sys/un.h>
#include <list>

namespace vio {
	class uds;
}

class vio::uds : public file {
	void open(file_t) {
	}
	void open(file_t, mode_t) {
	}
public:
	enum {
		FILEPATH,
		SOCKET,
		CONNECT,
		UNLINK,
		BIND,
		LISTEN,
		GETSOCKOPT,
		ACCEPT
	};
	typedef std::function<void(uds& uds_r)> hosted_t;
	typedef std::function<void()> joined_t;
	typedef std::function<void(const error& error)> denied_t;
	uds();
	uds(file_t file, joined_t joined, denied_t denied);
	uds(file_t file, hosted_t hosted);
	uds(file_t file, mode_t mode, hosted_t hosted);
	void subset(set& set_r);
	void drop();
	void join(file_t file);
	void host(file_t file);
	void host(file_t file, mode_t mode);
	void shut();
	~uds();
	hosted_t hosted;
	joined_t joined;
	denied_t denied;
private:
	typedef std::list<uds> subs_t;
	/*thread_local*/ static subs_t subs;
	subs_t::iterator sub_p;
	io::set* subset_p;
	sockaddr_un data;
	socklen_t size;
	static const state_t
	ESTABLISH = 0x40,
	LISTENING = 0x80;
	void readable();
	void writable();
};

#endif
